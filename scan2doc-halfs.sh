#! /bin/bash

# For dir of scans manuals -
# assumes scanned in sections, fronts first (upside down), backs reversed, but right-side-up

# requirements: graphicsmagick, poppler utils

# optional parm of completed PDF name, else uses base dir as pdf name

myname=`realpath  "$0"`
mypath=`dirname "$myname"`


basedir="$(basename "$(pwd)")"
test "$1" && name="$1" || name="$basedir"

num=`ls -1 *.pdf | wc -l`
half=$(($num / 2))

echo "$num files"  # "$half $(($num % 2)) `test $(($num % 2)) -eq 0;echo $?`

for f in *.pdf; do echo $f `qpdf --show-npages "$f"`; done

test $(($num % 2)) -eq 0 || { echo "Must be an even number of files" && exit 1; }


# make work dirs
# final dest is $name.pdf
# 1) rotate & reverse - qpdf
# 2) separate w/%d - pdfseparate
# 3) extract & crop - pdftoppm
# 4) recombine & compress - gm

mkdir -p .fronts .backs .work

i=0
for f in *.pdf; do
  if test $i -lt $half; then
    echo "Rotating front pages 180 in $f..."
    qpdf --rotate=180:1-z "$f" .fronts/"$f"
  else
    #echo "Reversing back pages in $f..."
    #qpdf --rotate=0:z-1 "$f" .backs/"$f"
    echo "Copying back pages in $f to .backs/..."
    cp "$f" .backs/
  fi
  ((i++))
done


# archive existing files
#echo "Archiving original files..."
#7z a -sdel "$name.org.7z" *.pdf


echo "Combining fronts..."
pdfunite .fronts/*.pdf fronts.pdf

echo "Combining backs..."
pdfunite .backs/*.pdf .work/backs-toreverse.pdf

echo "Reversing back pages..."
qpdf --pages=z-1 .work/backs-toreverse.pdf z-1 -- --empty backs.pdf


echo "Interleaving fronts & backs..."
"$mypath"/interleave.sh fronts.pdf backs.pdf && \
mv combined.pdf "$name.pdf" && \
rm .fronts/* && rm .backs/* && rm .work/* && rmdir .fronts .backs .work && rm fronts.pdf && rm backs.pdf && \
echo Done.
