#!/bin/bash

# for narrower small manuals, eg T27, 5.5"x8.5"
#startx=825
#width=825

# for wider small manuals, eg DTS, 6.5"x8.5" - rotate right 685, left 0
#startx=0
startx=685
width=985

for f in "$@"; do
  echo "Rotating $f"
  qpdf --rotate=90:1-z "$f" rotated-"$f"

  echo "Cropping $f"
  pdftoppm rotated-"$f" -x $startx -W $width "$f"
  #pdftoppm "$1"2.pdf -x 0 -W 985 "$1"

  echo "Converting ppm(s) back to $f"
  gm convert -compress JPEG *.ppm final-"$f"

  echo "Removing temp ppm files"
  rm *.ppm
done