#!/bin/bash

for ext in pdf PDF; do
  for f in *.$ext; do 
    if test "$f" = "*.$ext"; then exit 2; fi
    echo "$f"
    base=$(basename "$f" .$ext)
    echo "$base"
    convert -thumbnail x300 "$f"[0] "$base-thumb".png
  done
done


exit



old:

for f in *.pdf; do 
  # nope: base=`basename "$f" | cut -d. -f-99`
  echo "1: $f"
  base=$(basename "$f" .pdf)
  echo "1: $base"
  convert -thumbnail x300 "$f"[0] "$base-thumb".png
done

for f in *.PDF; do 
  if test "$f" = "*.PDF"; then exit 2; fi
  # nope: base=`basename "$f" | cut -d. -f-99`
  echo "2: $f"
  base=$(basename "$f" .PDF)
  echo "2: $base"
  convert -thumbnail x300 "$f"[0] "$base-thumb".png
done
