echo $0 - Setup script - install Hugo, graphicsmagick and ImageMagick

sudo apt-get -y update && sudo apt-get -y install ImageMagick graphicsmagick

wget https://github.com/gohugoio/hugo/releases/download/v0.73.0/hugo_0.73.0_Linux-64bit.deb

dpkg -i *.deb

# don't forget poppler tools, qpdf, graphicsmagick, etc for pdf processing - but those aren't needed in gitlab-ci