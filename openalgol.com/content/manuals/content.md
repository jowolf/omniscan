+++
fragment = "content"
#disabled = true
date = "2020-07-12"
weight = 100
#background = ""

title = "Reference Manuals and More"
#subtitle = "Algol or Burroughs/Unisys-Related Reference Manuals and Relevant Documentation"
+++

### Burroughs B 5000 / B 6000 / B 7000 Manuals

Scanned double-sided, with covers

Best efforts were made to preserve the original document, intent, and all pages, and covers

PCNs (Publication Change Notices) have sometimes been merged, and are sometimes separate

**Provided by Joe at The Libre Group**


### Burroughs

#### B 5000, B 6000, B 7000 Series

{{< folderpdfgallery src="Burroughs/B 5000, B 6000, B 7000 Series/Master Index" >}}
{{< folderpdfgallery src="Burroughs/B 5000, B 6000, B 7000 Series/B 6700 Information Processing Systems (IPS) - Reference Manual" >}}
{{< folderpdfgallery src="Burroughs/B 5000, B 6000, B 7000 Series/B 6800 System Reference Manual" >}}
{{< folderpdfgallery src="Burroughs/B 5000, B 6000, B 7000 Series/B 7700 Information Processing Systems (IPS) - Reference Manual" >}}
{{< folderpdfgallery src="Burroughs/B 5000, B 6000, B 7000 Series/Operator Display Terminal (ODT) - Operation Handbook" >}}


##### PL-1, Pascal Reference Manual and PCNs

{{< folderpdfgallery src="Burroughs/B 5000, B 6000, B 7000 Series/PL-1 Reference Manual" >}}
{{< folderpdfgallery src="Burroughs/B 5000, B 6000, B 7000 Series/Pascal Programming Reference Manual" >}}


#### Misc

##### RT 650 Automatic Teller Machine

{{< folderpdfgallery src="Burroughs/RT 650 Automatic Teller Machine" >}}

{{< disqus >}}
