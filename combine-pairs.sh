#! /bin/bash

# Alternately rotate and crop left, right, left right..
# 
# Should call check-files first

n=$((`ls -1 | wc -l` % 2))

myname=`realpath  "$0"`
mypath=`dirname "$myname"`

ls *.pdf &> /dev/null || { echo "No pdf files" && exit 1; }


if test $n==0; then
  echo "Even number of files - check"
else 
  echo "Must be an even number of files"
  exit 2
fi

set -x

i=0
for f in *.pdf; do
  ((i++))
  #echo $i $f $fprev
  if test $(($i % 2)) -eq 0; then
    "$mypath"/rotateleft-6.sh "$f" || exit 3
    "$mypath"/reverse.sh "final-$f"
    "$mypath"/rotateright-6.sh "$fprev" || exit 4
    "$mypath"/interleave.sh "final-$fprev" "reversed-final-$f"
    mv combined.pdf "combined-$f"
  fi
  fprev="$f"
done

echo OK

#rm reversed-*.pdf
#rm rotated-*.pdf
#rm final-*.pdf

pdfshuffler combined-*.pdf

exit


