#! /bin/bash

# Reverse order of pages in pdf file

set -x

test -z "$1" && { echo file name required; exit 1; }

echo Reversing "$1"

qpdf --empty --pages "$1" z-1 -- tmp
mv tmp "reversed-$1"


