#! /bin/bash

# Rename from HP00nn.pdf to scan00nn.pdf (older HP scan-to-USB printer in network closet)

# simpler:

i=161

for f in HP*.pdf; do
  mv "$f" scan`seq -w 0$i 0$i`.pdf
  ((i++))
done

echo OK

exit



# old:

base=161
i=0

for f in HP*.pdf; do
  echo mv "$f" scan`seq -w 0$(($base+$i)) 0$(($base + $i))`.pdf
  ((i++))
done
