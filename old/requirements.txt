#### pelican:

pelican
pelican[Markdown]

# for pdf-img plugin:
bs4
wand


#### nikola

#apt: python3-aiohttp/bionic, python3-watchdog/bionic

aiohttp
watchdog

# nope, can't do them all at once - pip isn't smart enough to sort out the dependencies / precedence
#pip install -U setuptools wheel nikola "Nikola[extras]"

setuptools
wheel
nikola
Nikola[extras]



