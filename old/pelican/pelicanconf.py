#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = 'Joseph Wolff'
SITENAME = 'OpenAlgol.com'
SITEURL = ''

PATH = 'content'

TIMEZONE = 'America/Los_Angeles'

DEFAULT_LANG = 'en'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = (('Pelican', 'http://getpelican.com/'),
         ('Python.org', 'http://python.org/'),
         ('Jinja2', 'http://jinja.pocoo.org/'),
         ('You can modify those links in your config file', '#'),)

# Social widget
SOCIAL = (('You can add links in your config file', '#'),
          ('Another social link', '#'),)

DEFAULT_PAGINATION = 10

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True


# JJW 7/7/20
#THEME = 'nest'
#THEME = '/home/joe/nikola-test/env/lib/python3.6/site-packages/pelican/themes/nest'
THEME = '../pelican-themes/nest/'

PLUGIN_PATHS = ['../pelican-plugins/']
PLUGINS = ['pdf-img',]

# plural appears wrong:
NEST_HEADER_IMAGES = 'Burroughs B6700 panel.jpg'
#'Burroughs B5500 panel.jpg'
#'Burroughs B6700 panel.jpg'

NEST_HEADER_LOGO = '/images/logo.jpg'

ARTICLE_ORDER_BY = 'slug'
