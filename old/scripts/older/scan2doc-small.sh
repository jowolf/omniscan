#! /bin/bash

# For massaging scans of smaller 6.5x8.5" manuals -
# assumes scanned in sections, fronts first, right edge in the feeder
# (Scanning away from the staples is better, or away from the binder holes)

# requirements: graphicsmagick, poppler utils

# optional parm of completed PDF name

basedir="$(basename "$(pwd)")"
test "$1" && name="$1" || name="$basedir"

num=`ls -1 *.pdf | wc -l`
half=$(($num / 2))


# make work dir, extract ppms

mkdir -p .work .work/.work

i=0
for f in *.pdf; do
  echo "Extracting ppms from $f..."
  pdftoppm "$f" .work/f$i
  ((i++))
done


# archive existing files
echo "Archiving original files..."
7z a -sdel "$name.org.7z" *.pdf


# rotate -90 deg & clip left to make small page
# could do 90, reduce it to upside-down to feed into org scan2doc, but then the cut is on the wrong side
# so leave it for now
echo "Rotating files -90 degrees..."
for f in .work/*.ppm; do pnmrotate -90 "$f" | pnmcut -left 666 -verbose >.work/"$f"; done


echo "Merging / Converting to $name.pdf (with jpg compression)"
gm convert -compress JPEG .work/.work/*.ppm "$name.pdf"
# -verbose


# rm -rf .work/

exit



history:


1417  pdftoppm scan0006.pdf .work/f1
 1418  man pnmrotate
 1419  mkdir .work2
 1420  for f in *.ppm; do pnmrotate 90 "$f" >.work2/"$f"; done
 1421  mkdir .work2
 1422  for f in *.ppm; do pnmrotate -90 "$f" >.work2/"$f"; done
 1423  man pnmcrop
 1424  mkdir .work3
 1425  man pnmcrop
 1426  for f in *.ppm; do pnmcrop "$f" >.work3/"$f"; done
 1427  man pnmcrop
 1428  for f in *.ppm; do pnmcrop -sides -verbose "$f" >.work3/"$f"; done
 1429  for f in *.ppm; do pnmcrop -white  -verbose "$f" >.work3/"$f"; done
 1430  for f in *.ppm; do pnmcrop -left -verbose "$f" >.work3/"$f"; done
 1431  for f in *.ppm; do pnmcrop -sides -left -verbose "$f" >.work3/"$f"; done
 1432  man pnmcut
 1433  for f in *.ppm; do pnmcut -left 665 -verbose "$f" >.work3/"$f"; done
 1434  for f in *.ppm; do pnmcut -left 666 -verbose "$f" >.work3/"$f"; done
 1435  history |tail -40


