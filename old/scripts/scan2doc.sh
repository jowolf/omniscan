#! /bin/bash

# assumes exactly 2 files, same # of pages - 1st front-to-back, 2nd back-to-front
# both upside down (Scanning from the bottom is usually better, esp w/de-stapled docs)


# optional parm of completed PDF name

basedir="$(basename "$(pwd)")"
test "$1" && name="$1" || name="$basedir"



# make work dir, rotate 180 deg

mkdir -p .work

for f in *.pdf; do
  qpdf --rotate=180:1-z "$f" ".work/normalized-$f"
done


# archive existing files

7z a "$name.work.7z" *.pdf


# reverse page order -  assumes back-to-front pdf is 2nd, uses leftover variable

f=".work/normalized-$f"

echo "$f"
qpdf --empty --pages "$f" z-1 -- tmp
mv tmp "$f"


# slpit & recombine files by sort order ("zip" style), interleave names at split

i=0
for f in .work/*.pdf; do
  #pdfimages $f pages/file$i.pdf
  pdfseparate $f ".work/page%03d-file$i.pdf"
  ((i++))
done


echo "$name"

#pnm2pdf
pdfunite .work/page* "$name.pdf"

exit



history:

qpdf --rotate=180:z-1 --split-pages=z-1 scan0001.pdf pages/page%d-filen.pdf
 1221  qpdf --help
 1222  qpdf --split-pages=z-1 scan0001.pdf pages/page%d-filen.pdf
 1223  qpdf --split-pages="z-1" scan0001.pdf pages/page%d-filen.pdf
 1224  qpdf --split-pages=z scan0001.pdf pages/page%d-filen.pdf
 1225  qpdf --split-pages=1 scan0001.pdf pages/page%d-filen.pdf
 1226  qpdf --show-pages scan0001.pdf 
 1227  qpdf --show-npages scan0001.pdf 
 1228  qpdf --help |less
 1229  man pdftops
 1230  man pdfunite 
 1231  ps
 1232  qpdf --show-npages scan0001.pdf z-1 outfile.pdf
 1233  qpdf --show-npages scan0001.pdf z-1 -- outfile.pdf
 1234  qpdf --pages scan0001.pdf z-1 -- outfile.pdf
 1235  qpdf --empty --pages scan0001.pdf z-1 -- outfile.pdf

# list embedded files
# pdfdetach -list scan.pdf 

# pdfimages - list or separate
# pdfimages -list scan.pdf 


# pdf2ppm works great too
