#! /bin/bash

# For massaging scans of smaller 6.5x8.5" manuals -
# assumes scanned in sections, fronts first, right edge in the feeder
# (Scanning away from the staples is better, or away from the binder holes)

# requirements: graphicsmagick, poppler utils

# optional parm of completed PDF name

basedir="$(basename "$(pwd)")"
test "$1" && name="$1" || name="$basedir"

num=`ls -1 *.pdf | wc -l`
half=$(($num / 2))

echo here $num $half $(($num % 2)) `test $(($num % 2)) -eq 0;echo $?`

#test $(($num % 2)) -eq 0 || { echo "Must be an even number of files" && exit 1 }

echo there


# make work dirs, extract ppms
# final dest is .work / pagen-filen-1front/2back
# 1) rotate & reverse - qpdf
# 2) separate w/%d - pdfseparate
# 3) extract & crop - pdftoppm
# 4) recombine & compress - gm

mkdir -p .fronts .backs .work

i=0
for f in *.pdf; do
  if test $i -lt $half; then
    echo "Rotating front pages from $f..."
    qpdf --rotate=90:1-z "$f" ".fronts/$f"
    #pdftoppm "$f" .fronts/f$i
  else
    echo "Rotating & reversing back pages in $f..."
    qpdf --split-pages=z-1 "$f" .backs/"$f"
    qpdf --rotate=-90:1-z --pages .backs/"$f" z-1 -- --empty tmp
    mv tmp ".backs/$f"
    #pdftoppm .backs/"$f" .backs/f$i
  fi
  ((i++))
done


# archive existing files
#echo "Archiving original files..."
#7z a -sdel "$name.org.7z" *.pdf


# fronts: rotate -90 deg & clip left extra to make small page
# backs: rotate +90
#echo "Rotating front files -90 degrees..."
#for f in .fronts/*.ppm; do pnmrotate -90 "$f" | pnmcut -left 666 -verbose >.work/"$f"; done
#echo "Rotating back files 90 degrees..."
#for f in .backs/*.ppm; do pnmrotate 90 "$f" | pnmcut -left 666 -verbose >.work/"$f"; done


# split / separate files by sort order - pagen-filen-1front or 2back

i=0
for f in .fronts/*.pdf; do
  echo "Separating front file $f"
  pdfseparate "$f" ".work/page%03d-file$i-1front.pdf"
  ((i++))
done

i=0
for f in .backs/*.pdf; do
  echo "Separating back file $f"
  pdfseparate "$f" ".work/page%03d-file$i-2back.pdf"
  ((i++))
done


# extract to ppm & crop -

i=0
for f in .work/*.pdf; do
  test $((i % 2)) -eq 0 && x=665 || x=0
  echo "Extracting $f to ppm with crop at $x..."
  pdftoppm "$f" -x $x -W 985 "$f"
  # -singlefile - nfg, writes to stdout
  ((i++))
done



exit


echo "Merging / Converting to $name.pdf (with jpg compression)"
gm convert -compress JPEG .work/.work/*.ppm "$name.pdf"
# -verbose


# rm -rf .work/

exit



history:


1417  pdftoppm scan0006.pdf .work/f1
 1418  man pnmrotate
 1419  mkdir .work2
 1420  for f in *.ppm; do pnmrotate 90 "$f" >.work2/"$f"; done
 1421  mkdir .work2
 1422  for f in *.ppm; do pnmrotate -90 "$f" >.work2/"$f"; done
 1423  man pnmcrop
 1424  mkdir .work3
 1425  man pnmcrop
 1426  for f in *.ppm; do pnmcrop "$f" >.work3/"$f"; done
 1427  man pnmcrop
 1428  for f in *.ppm; do pnmcrop -sides -verbose "$f" >.work3/"$f"; done
 1429  for f in *.ppm; do pnmcrop -white  -verbose "$f" >.work3/"$f"; done
 1430  for f in *.ppm; do pnmcrop -left -verbose "$f" >.work3/"$f"; done
 1431  for f in *.ppm; do pnmcrop -sides -left -verbose "$f" >.work3/"$f"; done
 1432  man pnmcut
 1433  for f in *.ppm; do pnmcut -left 665 -verbose "$f" >.work3/"$f"; done
 1434  for f in *.ppm; do pnmcut -left 666 -verbose "$f" >.work3/"$f"; done
 1435  history |tail -40


