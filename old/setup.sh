echo $0 - Setup venv script - ensures venv, pip, docker-compose present in env

sudo apt-get -y update && sudo apt-get -y install python3-venv graphicsmagick

/usr/bin/python3 -m venv env || exit 2
source env/bin/activate || exit 3


# pelican:

git clone --recursive https://github.com/getpelican/pelican-plugins.git
git clone --recursive https://github.com/getpelican/pelican-themes.git


# nikola:

# nope, can't do them all at once - pip isn't smart enough to sort out the dependencies / precedence
#pip install -U setuptools wheel nikola "Nikola[extras]"

pip install -r requirements.txt
