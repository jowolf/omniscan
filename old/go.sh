#!/usr/bin/env python3

import sys, os, fcntl, termios

# see:
# http://stackoverflow.com/questions/29614264/unable-to-fake-terminal-input-with-termios-tiocsti
# http://stackoverflow.com/questions/6191009/python-finding-stdin-filepath-on-linux

lines = '''\
. env/bin/activate
cd %s
''' % (sys.argv [1] if len (sys.argv) >1 else 'site')

tty = os.ttyname(sys.stdin.fileno())

with open(tty, 'w') as fd:
    for l in lines.split ('\n'):
        for c in l:
            fcntl.ioctl(fd, termios.TIOCSTI, c)

        fcntl.ioctl(fd, termios.TIOCSTI, '\n')

