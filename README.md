# OmniScan - https://gitlab.com/jowolf/omniscan

July, 2020

no more Nikola, Pelican - Now Hugo / Syna

TODO:
- watermark with ImageMagick pdf->jpg->watermarked->pdf 
  - save originals, improve compression/pdf version later, protect?
- spider subdirs with makethumbs
- spider subdirs in shortcodes/folderpdfgallery
- look into improving wfl for scans, esp smaller sizes
- fix original foldergallery, back to non-pdf images, actually gen thumbnails?


June-July 2020

Try Pelican - partial sccuess, Big-B panels, logo


May, 2020

Scanning scripts, media, and content tree itself

Todo:

- Nikola-based static site, hosted with Gitlab pages

- discrete, small utils for transforming dirs of scanned files - we have a good start on this so far

- flip (rotate180, not mirror) (1 or many)
- rotate l/r 
- rotate l/r with crop (for 5x8s)
- combine
- reverse order
- interleave

- Append covers? PDFShuffle works pretty well for that, by hand
