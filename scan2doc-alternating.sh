#! /bin/bash

# For dir of scans manuals -
# assumes scanned in sections, fronts (upside down) and backs (reversed) alternating

# requirements: graphicsmagick, poppler utils

# optional parm of completed PDF name, else uses base dir as pdf name

myname=`realpath  "$0"`
mypath=`dirname "$myname"`


basedir="$(basename "$(pwd)")"
test "$1" && name="$1" || name="$basedir"

num=`ls -1 *.pdf | wc -l`
half=$(($num / 2))

echo "$num files"  # "$half $(($num % 2)) `test $(($num % 2)) -eq 0;echo $?`

for f in *.pdf; do echo $f `qpdf --show-npages "$f"`; done

test $(($num % 2)) -eq 0 || { echo "Must be an even number of files" && exit 1; }


"$mypath"/check-pairs.sh || exit 2



# make work dirs
# final dest is $name.pdf
# 1) rotate & combine w/qpdf & copy
# 2) reverse backs w/qpdf
# 3) combine into fronts & backs w/pdfunite
# 4) interleave & combine backs & fronts

#set -x

mkdir -p .fronts .backs .work

i=0
for f in *.pdf; do
  ((i++))
  echo $i $f $fprev
  if test $(($i % 2)) -eq 0; then
    echo "Rotating front pages 180 in $fprev..."
    qpdf --rotate=180:1-z "$fprev" .fronts/"$fprev"
    echo "Copying back pages in $f to .backs/..."
    cp "$f" .backs/
  fi
  fprev="$f"
done


# archive existing files
#echo "Archiving original files..."
#7z a -sdel "$name.org.7z" *.pdf


echo "Combining fronts..."
pdfunite .fronts/*.pdf fronts.pdf

echo "Reversing backs..."
for f in .backs/*.pdf; do
  # can't use outfile name, use tmp & move
  qpdf --empty --pages "$f" z-1 -- .backs/"reversed-`basename $f`"
  #mv tmp ./backs/"reversed-`basename $f`"
done

echo "Combining backs..."
pdfunite .backs/reversed-*.pdf backs.pdf


echo "Interleaving fronts & backs..."
"$mypath"/interleave.sh fronts.pdf backs.pdf && \
mv combined.pdf "$name.pdf" && \
rm .fronts/* && rm .backs/* && rm .work/* && rmdir .fronts .backs .work && rm fronts.pdf && rm backs.pdf && \
d="/media/nas4-photo/Done/`date --rfc-3339=seconds`" && \
sudo mkdir "$d" && sudo mv scan*.pdf "$d/"
sudo mv HP*.pdf "$d/"
echo Done.
