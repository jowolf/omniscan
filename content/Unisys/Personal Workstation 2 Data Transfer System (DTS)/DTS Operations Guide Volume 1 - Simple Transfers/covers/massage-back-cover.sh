#!/bin/bash

qpdf --rotate=+90:1-1 "$1".pdf "$1"2.pdf

pdftoppm "$1"2.pdf -x 685 -W 985 "$1"
#pdftoppm "$1"2.pdf -x 0 -W 985 "$1"

gm convert -compress JPEG *.ppm "$1"_final.pdf
