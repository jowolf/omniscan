#! /bin/bash

# Tests for 2 of each file, with same # of pages, in collating order

# Check for even number of files:

n=$((`ls -1 | wc -l` % 2))

if test $n==0; then
  echo "Even number of files - check"
else 
  echo "Must be an even number of files"
  exit 2
fi

#set -x

i=0
for f in *.pdf; do
  ((i++))
  if test $(($i % 2)) -eq 0; then
    echo "$fprev" `qpdf --show-npages "$fprev"`
    echo "$f" `qpdf --show-npages "$f"`
    if ! test `qpdf --show-npages "$fprev"` -eq `qpdf --show-npages "$f"`; then
      echo "Number of pages of file pairs must be equal: $fprev $f"
      exit 3
    fi
  fi
  fprev="$f"
done

echo OK
