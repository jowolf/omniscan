#!/bin/bash

# rotate 180deg into same file - takes 1 parm

echo "Rotating $1"
qpdf --rotate=-90:1-z "$1" "rotated-$1" && \
echo Renaming files... && \
mv "$1" "old-$1" && \
mv "rotated-$1" "$1" && \
echo Done

