#! /bin/bash

# Interleave 2 PDFs - assumes exactly 2 files, same # of pages - both already front-to-back

set -x

test -z "$1" && { echo Two filenames required; exit 1; }
test -z "$2" && { echo Two filenames required; exit 1; }


# make work dir

mkdir -p .work
rm .work/*

# split & recombine files by sort order ("zip" style), interleave names at split

pdfseparate "$1" ".work/page%03d-file1.pdf"
pdfseparate "$2" ".work/page%03d-file2.pdf"


# combine the pages

pdfunite .work/page* combined.pdf
